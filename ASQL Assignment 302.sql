USE master
GO

DROP DATABASE IF EXISTS Assignment2Q1
GO

CREATE DATABASE Assignment2Q1
GO

USE Assignment2Q1
GO

CREATE TABLE San_Pham
(
	Ma_SP INT PRIMARY KEY IDENTITY(1,1),
	Ten_SP NVARCHAR(50),
	Don_Gia MONEY
)

CREATE TABLE Khach_Hang
(
	Ma_KH INT PRIMARY KEY IDENTITY (1,1),
	Ten_KH NVARCHAR (50),
	Phone_No VARCHAR (10),
	Ghi_Chu NVARCHAR (100)
)

CREATE TABLE Don_Hang
(
	Ma_DH INT NOT NULL,
	Ngay_DH DATETIME,
	Ma_SP INT FOREIGN KEY REFERENCES San_Pham(Ma_SP) NOT NULL,
	Ma_KH INT FOREIGN KEY REFERENCES Khach_Hang(Ma_KH) NOT NULL
)

ALTER TABLE Don_Hang ADD PRIMARY KEY(Ma_DH, Ma_SP, Ma_KH)

insert into San_Pham  (Ten_SP, Don_Gia) values ('Hickory Smoke, Liquid', '$14.23');
insert into San_Pham  (Ten_SP, Don_Gia) values ('Lettuce - Green Leaf', '$15.82');
insert into San_Pham  (Ten_SP, Don_Gia) values ('Cheese - Oka', '$15.12');
insert into San_Pham  (Ten_SP, Don_Gia) values ('Pork - Inside', '$10.19');
insert into San_Pham  (Ten_SP, Don_Gia) values ('Beer - Maudite', '$4.63');

insert into Khach_Hang  (Ten_KH, Phone_No, Ghi_Chu) values ('Lind Wilbraham', '9717197814', 'Etiam justo.');
insert into Khach_Hang  (Ten_KH, Phone_No, Ghi_Chu) values ('Ameline Rosenfrucht', '4104523213', 'Nulla ac enim.');
insert into Khach_Hang  (Ten_KH, Phone_No, Ghi_Chu) values ('Manya Duding', '9069591743', 'Suspendisse potenti.');
insert into Khach_Hang  (Ten_KH, Phone_No, Ghi_Chu) values ('Amalea Talman', '8747768442', 'Aenean lectus.');
insert into Khach_Hang  (Ten_KH, Phone_No, Ghi_Chu) values ('Emmit Crowcum', '8394250833', 'Nullam porttitor lacus at turpis.');

insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (1, '03/31/2023', 1, 1);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (1, '11/09/2022', 5, 1);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (1, '05/18/2023', 3, 1);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (2, '04/17/2023', 3, 2);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (3, '04/18/2023', 2, 3);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (4, '10/31/2022', 2, 4);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (5, '03/13/2023', 1, 5);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (5, '01/11/2023', 2, 5);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (3, '07/20/2023', 5, 3);
insert into Don_Hang (Ma_DH, Ngay_DH, Ma_SP, Ma_KH) values (3, '07/17/2023', 3, 3);

CREATE VIEW DON_HANG AS
SELECT dh.Ma_DH, kh.Ma_KH ,kh.Ten_KH, dh.Ngay_DH, sp.Ma_SP, SUM(sp.Don_Gia) as 'Total Price'
FROM Khach_Hang kh
INNER JOIN Don_Hang dh ON kh.Ma_KH = dh.Ma_KH
INNER JOIN San_Pham sp ON dh.Ma_SP = sp.Ma_SP
GROUP BY dh.Ma_DH, kh.Ma_KH, kh.Ten_KH, dh.Ngay_DH, sp.Ma_SP
----------------------QUESTION2--------------------------
CREATE TABLE Employee_Table
(
	Employee_Number INT PRIMARY KEY IDENTITY (1,1),
	Employee_Name NVARCHAR(50),
	Department_Number INT FOREIGN KEY REFERENCES Department(Department_Number)
)

CREATE TABLE Skill_Table
(
	Skill_Code INT PRIMARY KEY IDENTITY(1,1),
	Skill_Name VARCHAR (50)
)

CREATE TABLE Employee_Skill_Table
(
	Employee_Number INT FOREIGN KEY REFERENCES Employee_Table(Employee_Number) NOT NULL,
	Skill_Code INT FOREIGN KEY REFERENCES Skill_Table(Skill_Code) NOT NULL,
	Date_Registered DATETIME
)
ALTER TABLE Employee_Skill_Table ADD PRIMARY KEY (Employee_Number,Skill_Code)
CREATE TABLE Department
(
	Department_Number INT PRIMARY KEY IDENTITY (1,1),
	Department_Name NVARCHAR(50)
)

SELECT * FROM Employee_Table

insert into Skill_Table (Skill_Name) values ('C#');
insert into Skill_Table (Skill_Name) values ('.NET');
insert into Skill_Table (Skill_Name) values ('C++');
insert into Skill_Table (Skill_Name) values ('JAVA');
insert into Skill_Table (Skill_Name) values ('SAP');

insert into Department (Department_Name) values ('Engineering');
insert into Department (Department_Name) values ('Marketing');
insert into Department (Department_Name) values ('Research and Development');
insert into Department (Department_Name) values ('Accounting');
insert into Department (Department_Name) values ('Human Resources');


insert into Employee_Table (Employee_Name, Department_Number) values ('Esther Sully', 5);
insert into Employee_Table (Employee_Name, Department_Number) values ('Cassey Instone', 5);
insert into Employee_Table (Employee_Name, Department_Number) values ('Lonnie Gimlet', 3);
insert into Employee_Table (Employee_Name, Department_Number) values ('Hugues Cahill', 3);
insert into Employee_Table (Employee_Name, Department_Number) values ('Cassandra Yeell', 5);

insert into Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered) values (6, 2, '10/31/2022');
insert into Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered) values (7, 3, '10/17/2022');
insert into Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered) values (8, 1, '06/18/2023');
insert into Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered) values (8, 2, '05/14/2023');
insert into Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered) values (10, 4, '10/15/2022');

SELECT e.Employee_Name
FROM Employee_Table e
INNER JOIN Employee_Skill_Table est ON e.Employee_Number = est.Employee_Number
INNER JOIN Skill_Table st ON est.Skill_Code = st.Skill_Code
WHERE st.Skill_Name = 'JAVA'

SELECT e.Employee_Name 
FROM Employee_Table e
INNER JOIN Employee_Skill_Table est ON e.Employee_Number = est.Employee_Number
WHERE est.Skill_Code = (SELECT st.Skill_Code FROM Skill_Table st WHERE st.Skill_Name = 'JAVA')

SELECT d.Department_Name, COUNT(e.Employee_Name) as 'Number of employee'
FROM Department d
INNER JOIN Employee_Table e ON e.Department_Number = d.Department_Number
GROUP BY  d.Department_Name
HAVING  COUNT(e.Employee_Name) >= 3

SELECT e.Employee_Number, e.Employee_Name, COUNT(est.Employee_Number) AS 'Number of Skill'
FROM Employee_Table e
INNER JOIN Employee_Skill_Table est ON e.Employee_Number = est.Employee_Number
GROUP BY e.Employee_Number, e.Employee_Name
HAVING COUNT(est.Employee_Number) > 1

CREATE VIEW DIFFERENT_EMPLOYEE AS 
SELECT e.Employee_Number, e.Employee_Name, d.Department_Name ,COUNT(est.Employee_Number) AS 'Number of Skill'
FROM Employee_Table e
INNER JOIN Department d ON d.Department_Number = e.Department_Number
INNER JOIN Employee_Skill_Table est ON e.Employee_Number = est.Employee_Number
GROUP BY e.Employee_Number, e.Employee_Name, d.Department_Name
HAVING COUNT(est.Employee_Number) > 1